# Use a minimal base image
FROM debian:11.6-slim

# Set labels
LABEL maintainer="Prismatic Dev <dev@prismatic.io>"
LABEL updated="2023-03-20"

# Define environment variables
ENV SERVICE_USER=service \
    SERVICE_USER_HOME=/home/service \
    SERVICE_DIR=/service

# Use the recommended shell syntax
# This prevents potential issues caused by the default shell
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Create user, install packages, and clean up apt cache in one RUN command
# This reduces the number of layers in the image
RUN useradd -ms /bin/bash -d "${SERVICE_USER_HOME}" "${SERVICE_USER}" \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install --no-install-recommends --assume-yes \
    bash=5.1-2+deb11u1 \
    ca-certificates=20210119 \
    curl=7.74.0-1.3+deb11u7 \
    git=1:2.30.2-1+deb11u2 \
    gnupg=2.2.27-2+deb11u2 \
    jq=1.6-2.1 \
    make=4.3-4.1 \
    unzip=6.0-26+deb11u1 \
    zip=3.0-12 \
    sudo=1.9.5p2-3+deb11u1 \
    && apt-get -y upgrade \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && curl -Ls --tlsv1.2 --proto "=https" --retry 3 https://cli.doppler.com/install.sh | sh \
    && mkdir -p "${SERVICE_DIR}"
# && chown "${SERVICE_USER}:${SERVICE_USER}" "${SERVICE_DIR}"

# Switch to the SERVICE_USER
# USER ${SERVICE_USER}
WORKDIR ${SERVICE_DIR}
